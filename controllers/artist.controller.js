const ArtistService = require("../services/artist.service");
const ArtistController = {
    findAll: async (req, res, next) => {
        const valeur = await ArtistService.findAll();

        auteur=[]

        for(i=0;i<valeur.length;i++){
            auteur.push(valeur[i].auteur);
        }
        auteur= Array.from(new Set(auteur));
        result={titres:[],genres:[]};
        songauteur=[]
        for(i=0;i<auteur.length;i++) {
            titre=[];
            genre=[];
            for (j = 0; j < valeur.length; j++) {
                if(auteur[i]==valeur[j].auteur){
                    titre.push(valeur[j].titre)
                    genre.push(valeur[j].genre)
                }
            }

            result.titres=titre;
            result.genres=genre;
           songauteur[i]=result;

            result={};
        }
        song={};
        final={};
        reponse=[]
        for(i=0;i<auteur.length;i++) {
            final.name=auteur[i]
            song.title=songauteur[i].titres;
            song.genre=songauteur[i].genres;
            final.songs=song
            reponse.push(final);
            final={};
            song={};
        }
        res.status(200).send(reponse);

    }};
module.exports = ArtistController;
