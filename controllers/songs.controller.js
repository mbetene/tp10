const SongsService = require("../services/songs.service");
const SongsController = {
  findAll: async (req, res, next) => {
    const songs = await SongsService.findAll();
    res.status(200).send(songs);
  },
  findById: async (req, res, next) => {
    const songsId = req.params.guid;
    const songs = await SongsService.findById(songsId);
    res.status(200).send(songs);
  },
  create: async (req, res, next) => {
    const songs = req.body;
    await SongsService.create(songs);
    res.status(200).send({ message: "le songs a bien été crée" });
  },
  findByName: async (req, res, next) => {
    const query = req.query.q;
    const songs =  await SongsService.findByName(query);
    res.status(200).send(songs);
  },
  update: async (req, res, next) => {
    const songsId = req.params.guid;
    const songsInfo = req.body;
    try {
      await SongsService.update(songsId, songsInfo);
      res.status(200).send({ message: "songs mis à jour" });
    } catch (error) {
      res.status(404).send({ message: `le songs  ${songsId} n'a pas été trouver` });
    }
  }
};

module.exports = SongsController;
