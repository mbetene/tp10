module.exports = (sequelize, Sequelize) => {
  const Songs = sequelize.define("songs", {
    guid: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    genre: {
      type: Sequelize.STRING,
      validate: {
        min: 0,
        max: 10,
      }
    },
    titre: {
      type: Sequelize.STRING,
      validate: {
        min: 0,
        max: 80,
      }
    },
    duree: {
      type: Sequelize.INTEGER,
    },
    auteur: {
      type: Sequelize.STRING,
      validate: {
        min: 0,
        max: 80,
      }
    }
  });

  return Songs;
};
