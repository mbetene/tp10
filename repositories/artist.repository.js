const db = require("../models");
const Songs = db.songs;

const ArtistRepository = {
    findAll: () => {
        return Songs.findAll();
    }};
module.exports = ArtistRepository;
