const db = require("../models");
const Songs = db.songs;

const SongsRepository = {
  findAll: () => {
    return Songs.findAll();
  },
  findById: (guid) => {
    const songs = Songs.findByPk(guid);
    return songs;
  },
  create: (songs) => {
    return Songs.create(songs);
  },
  findByName: (name) => {
    const songs =Songs.findAll({ where: { auteur: name } });
    return songs;
  },
  update: (guid, songs) => {
    return Songs.update(songs, { where: { guid: guid } });
  }
};

module.exports = SongsRepository;
