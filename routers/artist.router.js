const express = require("express");
const artistRouter = express.Router();
const ArtistController = require("../controllers/artist.controller");


// Mise en place des routes
artistRouter
    .route("/")
    .get(ArtistController.findAll);

// fin des routes
module.exports = artistRouter;
