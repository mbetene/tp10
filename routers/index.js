const express = require("express");
const router = express.Router();
const SongsRouter = require("./songs.router");
const ArtistRouter = require("./artist.router");
const API_SONGS = `/songs`;
const API_ARTIST = `/artists`;

router.use(API_SONGS, SongsRouter);
router.use(API_ARTIST,ArtistRouter);

module.exports = router;
