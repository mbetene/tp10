const express = require("express");
const songsRouter = express.Router();
const SongsController = require("../controllers/songs.controller");
const { validate } = require("express-validation");
const SongsValidator = require("../validators/songs.validators");


// Mise en place des routes
songsRouter
  .route("/")
  .get(SongsController.findAll)
  .post(validate(SongsValidator.validateCreate), SongsController.create);

songsRouter.route(`/:guid`)
           .get(SongsController.findById)
           .put(validate(SongsValidator.validateUpdate), SongsController.update);


songsRouter.route(`/search/artists`).get(SongsController.findByName);


// fin des routes
module.exports = songsRouter;

