const ArtistRepository = require("../repositories/artist.repository");

const ArtistService = {
    findAll: async () => {
        return await ArtistRepository.findAll();
    }};

module.exports = ArtistService;
