const SongsRepository = require("../repositories/songs.repository");


const SongsService = {
  findAll: async () => {
    return await SongsRepository.findAll();
  },

  findById: async (guid) => {
    const songs = await SongsRepository.findById(guid);
    return songs;
  },
  create: async (songs) => {
    await SongsRepository.create(songs);
  },
  findByName: async (name) => {
    const songs =  await SongsRepository.findByName(name);
    return songs;
  },
  update: async (guid, songsInfo) => {
    const songs = await SongsRepository.findById(guid);
    if (!songs) {
      throw Error("songs absent");
    }
    await SongsRepository.update(guid, songsInfo);
  }
};

module.exports = SongsService;
