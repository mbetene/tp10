const { Joi } = require("express-validation");

  // Songs Validators


const SongsValidators = {
  validateCreate: {
    body: Joi.object({
      genre: Joi.string().min(0).max(80),
      titre: Joi.string().min(0).max(80).required(),
      duree: Joi.number().integer(),
      auteur: Joi.string().max(100).required()
    }),
  },
  validateUpdate: {
    params: Joi.object({
      guid: Joi.string().guid().required(),
    }),
    body: Joi.object({
      genre: Joi.string().min(0).max(80),
      titre: Joi.string().min(0).max(80).required(),
      duree: Joi.number().integer(),
      auteur: Joi.string().max(100).required()
    }),
  },
};

module.exports = SongsValidators;
